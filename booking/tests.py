from django.test import TestCase
from django.contrib.auth.models import User
from datetime import datetime, timedelta, date
from .forms import ReservationForm
import factory


class UserFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = User
    username = factory.Sequence(lambda n: 'user%d' % n)
    email = 'admin@admin.com'
    password = factory.PostGenerationMethodCall('set_password', 'adm1n')

    is_superuser = True
    is_staff = True
    is_active = True


class RoomFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = "booking.Room"
    number = factory.Sequence(lambda n: n)
    room_type = 0


class ReservationFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = "booking.Reservation"

    room = factory.SubFactory(RoomFactory)
    user = factory.SubFactory(UserFactory)
    name = factory.Sequence(lambda n: "Guess %s" % n)
    email = factory.Sequence(lambda n: "name.%s@gmail.com" % n)
    telephone = "6860000011"
    price = "50"
    special_request = ""
    have_pet = False
    confirmed = False
    clinic_guest = True
    birthday = date.today()
    color = 1
    clinic_selected = 0
    additional_notes = ""
    free_days = 0
    blocked_days = 0
    no_paid_days = 0
    start_date = date.today()
    end_date = date.today() + timedelta(days=3)
    created = datetime.now()


class BookingTestCase(TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_unicode_reservation(self):
        room = RoomFactory(number=1)
        reservation = ReservationFactory(
            room=room,
            name="Juanito Perez"
        )
        self.assertEqual(unicode(reservation), "[1] Double Room - Juanito Perez")

    def test_create_reservation(self):
        reservation = ReservationFactory()
        self.assertTrue(reservation.id is not None)

    def test_update_reservation(self):
        room = RoomFactory(number=1)
        reservation = ReservationFactory()
        self.assertTrue(reservation.id is not None)
        self.assertNotEqual(unicode(reservation), "[1] Double Room - Jesus Anaya")

        reservation.name = "Jesus Anaya"
        reservation.room = room
        self.assertEquals(unicode(reservation), "[1] Double Room - Jesus Anaya")

    def test_delete_reservation(self):
        reservation = ReservationFactory()
        self.assertTrue(reservation.id is not None)

        reservation.delete()
        self.assertTrue(reservation.id is None)

    def test_validate_fields_reservation(self):
        reservation = ReservationFactory(name="", email="")
        form = ReservationForm(instance=reservation)
        self.assertFalse(form.is_valid())

    def test_validate_permissions_reservation(self):
        pass

