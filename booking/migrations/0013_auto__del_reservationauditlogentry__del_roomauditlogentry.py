# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'ReservationAuditLogEntry'
        db.delete_table(u'booking_reservationauditlogentry')

        # Deleting model 'RoomAuditLogEntry'
        db.delete_table(u'booking_roomauditlogentry')


    def backwards(self, orm):
        # Adding model 'ReservationAuditLogEntry'
        db.create_table(u'booking_reservationauditlogentry', (
            ('color', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('telephone', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('action_user', self.gf('audit_log.models.fields.LastUserField')(related_name='_reservation_audit_log_entry')),
            ('special_request', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            (u'id', self.gf('django.db.models.fields.IntegerField')(blank=True, db_index=True)),
            ('confirmed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('free_days', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('start_date', self.gf('django.db.models.fields.DateField')()),
            ('action_id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('no_paid_days', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('blocked_days', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('end_date', self.gf('django.db.models.fields.DateField')()),
            ('price', self.gf('django.db.models.fields.FloatField')(default=0, blank=True)),
            ('birthday', self.gf('django.db.models.fields.CharField')(default='', max_length=50, blank=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True, blank=True)),
            ('clinic_selected', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('room', self.gf('django.db.models.fields.related.ForeignKey')(related_name='_auditlog_reservations', to=orm['booking.Room'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('have_pet', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('action_date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('action_type', self.gf('django.db.models.fields.CharField')(max_length=1)),
        ))
        db.send_create_signal(u'booking', ['ReservationAuditLogEntry'])

        # Adding model 'RoomAuditLogEntry'
        db.create_table(u'booking_roomauditlogentry', (
            ('action_date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('number', self.gf('django.db.models.fields.IntegerField')()),
            ('action_type', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('action_user', self.gf('audit_log.models.fields.LastUserField')(related_name='_room_audit_log_entry')),
            (u'id', self.gf('django.db.models.fields.IntegerField')(blank=True, db_index=True)),
            ('room_type', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('action_id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'booking', ['RoomAuditLogEntry'])


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'booking.log': {
            'Meta': {'ordering': "('-created',)", 'object_name': 'Log'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'table': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'booking.reservation': {
            'Meta': {'ordering': "('-start_date',)", 'object_name': 'Reservation'},
            'birthday': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50', 'blank': 'True'}),
            'blocked_days': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'clinic_selected': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'color': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'confirmed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'end_date': ('django.db.models.fields.DateField', [], {}),
            'free_days': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'have_pet': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'no_paid_days': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'price': ('django.db.models.fields.FloatField', [], {'default': '0', 'blank': 'True'}),
            'room': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'reservations'", 'to': u"orm['booking.Room']"}),
            'special_request': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {}),
            'telephone': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'})
        },
        u'booking.room': {
            'Meta': {'ordering': "('number',)", 'object_name': 'Room'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {}),
            'room_type': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['booking']