# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'RoomDay.color'
        db.add_column(u'booking_roomday', 'color',
                      self.gf('django.db.models.fields.CharField')(default='#FFA500', max_length=10),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'RoomDay.color'
        db.delete_column(u'booking_roomday', 'color')


    models = {
        u'booking.reservation': {
            'Meta': {'object_name': 'Reservation'},
            'free_days': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'have_pet': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'special_request': ('django.db.models.fields.TextField', [], {})
        },
        u'booking.room': {
            'Meta': {'ordering': "('number',)", 'object_name': 'Room'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {}),
            'room_type': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'booking.roomday': {
            'Meta': {'ordering': "('date',)", 'object_name': 'RoomDay'},
            'color': ('django.db.models.fields.CharField', [], {'default': "'#FFA500'", 'max_length': '10'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reservation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.Reservation']"}),
            'room': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.Room']"})
        }
    }

    complete_apps = ['booking']