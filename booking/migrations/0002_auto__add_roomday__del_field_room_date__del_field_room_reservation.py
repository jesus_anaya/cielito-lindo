# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'RoomDay'
        db.create_table(u'booking_roomday', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('room', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['booking.Room'])),
            ('reservation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['booking.Reservation'])),
            ('date', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal(u'booking', ['RoomDay'])

        # Deleting field 'Room.date'
        db.delete_column(u'booking_room', 'date')

        # Deleting field 'Room.reservation'
        db.delete_column(u'booking_room', 'reservation_id')


    def backwards(self, orm):
        # Deleting model 'RoomDay'
        db.delete_table(u'booking_roomday')

        # Adding field 'Room.date'
        db.add_column(u'booking_room', 'date',
                      self.gf('django.db.models.fields.DateField')(default=None),
                      keep_default=False)

        # Adding field 'Room.reservation'
        db.add_column(u'booking_room', 'reservation',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['booking.Reservation']),
                      keep_default=False)


    models = {
        u'booking.reservation': {
            'Meta': {'object_name': 'Reservation'},
            'free_days': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'have_pet': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'special_request': ('django.db.models.fields.TextField', [], {})
        },
        u'booking.room': {
            'Meta': {'object_name': 'Room'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {}),
            'room_type': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'booking.roomday': {
            'Meta': {'object_name': 'RoomDay'},
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reservation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.Reservation']"}),
            'room': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.Room']"})
        }
    }

    complete_apps = ['booking']