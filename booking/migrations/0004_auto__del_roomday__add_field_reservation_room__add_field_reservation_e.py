# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'RoomDay'
        db.delete_table(u'booking_roomday')

        # Adding field 'Reservation.room'
        db.add_column(u'booking_reservation', 'room',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['booking.Room']),
                      keep_default=False)

        # Adding field 'Reservation.email'
        db.add_column(u'booking_reservation', 'email',
                      self.gf('django.db.models.fields.EmailField')(default='', max_length=75),
                      keep_default=False)

        # Adding field 'Reservation.blocked_days'
        db.add_column(u'booking_reservation', 'blocked_days',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Reservation.no_paid_days'
        db.add_column(u'booking_reservation', 'no_paid_days',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Reservation.start_date'
        db.add_column(u'booking_reservation', 'start_date',
                      self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2013, 11, 11, 0, 0)),
                      keep_default=False)

        # Adding field 'Reservation.end_date'
        db.add_column(u'booking_reservation', 'end_date',
                      self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2013, 11, 11, 0, 0)),
                      keep_default=False)


    def backwards(self, orm):
        # Adding model 'RoomDay'
        db.create_table(u'booking_roomday', (
            ('room', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['booking.Room'])),
            ('color', self.gf('django.db.models.fields.CharField')(default='#FFA500', max_length=10)),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('reservation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['booking.Reservation'])),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'booking', ['RoomDay'])

        # Deleting field 'Reservation.room'
        db.delete_column(u'booking_reservation', 'room_id')

        # Deleting field 'Reservation.email'
        db.delete_column(u'booking_reservation', 'email')

        # Deleting field 'Reservation.blocked_days'
        db.delete_column(u'booking_reservation', 'blocked_days')

        # Deleting field 'Reservation.no_paid_days'
        db.delete_column(u'booking_reservation', 'no_paid_days')

        # Deleting field 'Reservation.start_date'
        db.delete_column(u'booking_reservation', 'start_date')

        # Deleting field 'Reservation.end_date'
        db.delete_column(u'booking_reservation', 'end_date')


    models = {
        u'booking.reservation': {
            'Meta': {'object_name': 'Reservation'},
            'blocked_days': ('django.db.models.fields.IntegerField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'end_date': ('django.db.models.fields.DateField', [], {}),
            'free_days': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'have_pet': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'no_paid_days': ('django.db.models.fields.IntegerField', [], {}),
            'price': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'room': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.Room']"}),
            'special_request': ('django.db.models.fields.TextField', [], {}),
            'start_date': ('django.db.models.fields.DateField', [], {})
        },
        u'booking.room': {
            'Meta': {'ordering': "('number',)", 'object_name': 'Room'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {}),
            'room_type': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['booking']