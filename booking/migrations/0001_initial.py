# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Reservation'
        db.create_table(u'booking_reservation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('price', self.gf('django.db.models.fields.FloatField')(default=0.0)),
            ('free_days', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('special_request', self.gf('django.db.models.fields.TextField')()),
            ('have_pet', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'booking', ['Reservation'])

        # Adding model 'Room'
        db.create_table(u'booking_room', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('reservation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['booking.Reservation'])),
            ('number', self.gf('django.db.models.fields.IntegerField')()),
            ('room_type', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('date', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal(u'booking', ['Room'])


    def backwards(self, orm):
        # Deleting model 'Reservation'
        db.delete_table(u'booking_reservation')

        # Deleting model 'Room'
        db.delete_table(u'booking_room')


    models = {
        u'booking.reservation': {
            'Meta': {'object_name': 'Reservation'},
            'free_days': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'have_pet': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'special_request': ('django.db.models.fields.TextField', [], {})
        },
        u'booking.room': {
            'Meta': {'object_name': 'Room'},
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {}),
            'reservation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.Reservation']"}),
            'room_type': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['booking']