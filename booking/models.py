from django.db import models
from django.contrib.auth import get_user_model

DOUBLE_ROOM = 0
JUNIOR_SUITE = 1
SINGLE_ROOM = 2

ROOM_TYPE = (
    (DOUBLE_ROOM, "Double Room"),
    (JUNIOR_SUITE, "Junior Suite"),
    (SINGLE_ROOM, "Single Room"),
)

COLOR_TYPE = (
    (0, "Clinic Patient"),
    (1, "Normal Guest"),
    (2, "Guest with pet"),
    (3, "Handicap"),
    (4, "Blocked"),
    (5, "Reserved"),
    (6, "Reserved by Booking"),
    (7, "Reserved by Expedia"),
)

CLINICS = (
    (0, 'Sani Dental Group'),
    (1, 'Sani Dental Platinum'),
    (2, 'Class Dental Care'),
)

LOG_CREATED = 'Created'
LOG_MODIFIED = 'Modified'
LOG_DELETED = 'Deleted'

LOG = (LOG_CREATED, LOG_MODIFIED, LOG_DELETED)

User = get_user_model()


class Log(models.Model):
    table = models.CharField(max_length=100)
    description = models.TextField()
    user = models.ForeignKey(User)
    type = models.CharField(max_length=100, choices=zip(LOG, LOG))
    created = models.DateTimeField(auto_now=True)
    modified = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return "%s - %s - %s" % (self.table, self.user, self.type)


class Room(models.Model):
    class Meta:
        ordering = ('number',)

    number = models.IntegerField()
    room_type = models.IntegerField(default=0, choices=ROOM_TYPE)

    def room_type_name(self):
        return ROOM_TYPE[self.room_type][1]

    def __unicode__(self):
        return "[%d] %s" % (self.number, ROOM_TYPE[self.room_type][1])


class Reservation(models.Model):
    class Meta:
        ordering = ('-start_date',)
        index_together = (('name', 'email'),)

    room = models.ForeignKey(Room, related_name='reservations')
    user = models.ForeignKey(User, null=True, blank=True)
    name = models.CharField(max_length=255)
    email = models.EmailField()
    telephone = models.CharField(max_length=100, blank=True, null=True)
    price = models.FloatField(default=0, blank=True)
    special_request = models.TextField(default="", blank=True)
    have_pet = models.BooleanField(default=False)
    confirmed = models.BooleanField(default=False, verbose_name=u'Send confirmation')
    clinic_guest = models.BooleanField(default=False)
    birthday = models.CharField(max_length=50, default="", blank=True)
    color = models.IntegerField(default=1, choices=COLOR_TYPE)
    clinic_selected = models.IntegerField(default=0, blank=True, null=True, choices=CLINICS)
    additional_notes = models.TextField(blank=True, null=True)

    free_days = models.IntegerField(default=0, blank=True)
    blocked_days = models.IntegerField(default=0)
    no_paid_days = models.IntegerField(default=0)

    start_date = models.DateField()
    end_date = models.DateField()

    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return "%s - %s" % (self.room, self.name)

    def nights(self):
        return abs((self.end_date - self.start_date).days)

    def room_name(self):
        return "%s %s" % (self.room.number, self.room.room_type_name())

    def room_name_single(self):
        return "%s" % self.room.room_type_name()

    def need_special_request(self):
        return bool(len(self.special_request) > 0)

    ## Getters
    def get_nights(self):
        return self.nights()

    def get_room_type(self):
        if self.room.room_type == SINGLE_ROOM:
            return "single room (1 queen size bed)"
        elif self.room.room_type == DOUBLE_ROOM:
            return "Double room (2 queen size beds)"
        elif self.room.room_type == JUNIOR_SUITE:
            return "Junior suite (2 queen size bed & walking closet)"
        else:
            return "N/A"

