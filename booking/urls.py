from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from .views import (HomeView, LoginView, LogoutView, ReportView,
                    ProfileView, ProfilePasswordView)


urlpatterns = patterns('',
    url(r'^$', login_required(HomeView.as_view()), name="home"),
    url(r'^reports/$', login_required(ReportView.as_view()), name="reports"),
    url(r'^login/$', LoginView.as_view(), name="login"),
    url(r'^logout/$', LogoutView.as_view(), name="logout"),

    url(r'^profile/(?P<pk>\d+)/$',
                    login_required(ProfileView.as_view()), name="profile"),

    url(r'^password/$',
            login_required(ProfilePasswordView.as_view()), name="password"),
)
