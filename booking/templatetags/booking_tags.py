from django import template
from django.http import Http404

register = template.Library()

@register.inclusion_tag('tags/description.html')
def description_table(string):
    data = []
    for line in string.split(','):
        try:
            key, value = line.split(':')
            data.append({'key': key, 'value': value})
        except ValueError:
            pass

    return {'data': data}


@register.simple_tag
def validate_user(user, profile):
    if user.pk != profile.pk:
        raise Http404
    return ""
