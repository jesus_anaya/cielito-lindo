from django.views.generic import TemplateView, View, ListView
from django.views.generic.edit import FormView
from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth import login, logout, authenticate, get_user_model
from .models import Room, Log, COLOR_TYPE
from .forms import UserChangePassword

User = get_user_model()


class HomeView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['rooms'] = Room.objects.all()
        context['colors'] = COLOR_TYPE
        return context


class ProfileView(FormView):
    template_name = "profile.html"
    form_class = UserChangeForm
    success_url = '/'


class ProfilePasswordView(FormView):
    template_name = "password.html"
    form_class = UserChangePassword
    success_url = '/'

    def get_form_kwargs(self):
        kwargs = super(ProfilePasswordView, self).get_form_kwargs()
        kwargs['user'] = self.request.user;
        return kwargs


class ReportView(ListView):
    template_name = "report.html"
    context_object_name = "logs"
    model = Log
    paginate_by = 20

    def get(self, request, *args, **kwargs):
        #if request.GET.get('order'):
        #    order = request.GET.get('order')
        return super(ReportView, self).get(request, *args, **kwargs)


class LoginView(TemplateView):
    template_name = "login.html"

    def get(self, request, *args, **kwargs):
        if request.user.is_active:
            return redirect(reverse_lazy('home'))
        return super(LoginView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect(reverse_lazy('home'))

        return redirect(str(reverse_lazy('login')) + '?error=1')


class LogoutView(View):
    def dispatch(self, request, *args, **kwargs):
        logout(request)
        return redirect(reverse_lazy('login'))
