from south.modelsinspector import add_introspection_rules
from django.contrib.auth.models import User
from audit_log.models import fields


rules = [((fields.LastUserField,), [], {
    'to': ['rel.to', {'default': User}],
    'null': ['null', {'default': True}],
},)]

# Add the rules for the `LastUserField`
add_introspection_rules(rules, ['^audit_log\.models\.fields\.LastUserField'])
