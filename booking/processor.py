from datetime import date


def current_date(request):
    return {'current_date': str(date.today())}
