from django.core.management.base import BaseCommand, CommandError
from datetime import timedelta
from booking.models import Reservation


class Command(BaseCommand):
    def handle(self, *args, **options):
        for reservation in Reservation.objects.all():
            reservation.end_date += timedelta(days=1)
            reservation.save()
            print("Change end_date to %s", reservation)
