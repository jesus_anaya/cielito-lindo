from django import forms
from django.contrib.auth.forms import PasswordChangeForm
from .models import Reservation


class ReservationForm(forms.ModelForm):
    class Meta:
        model = Reservation


class UserChangePassword(PasswordChangeForm):

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 == password2:
                self.user.set_password(password2)
                self.user.save()
            else:
                raise forms.ValidationError(
                    self.error_messages.get('password_mismatch'),
                    code='password_mismatch',
                )
        return password2
