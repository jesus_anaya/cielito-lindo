from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    is_receptionist = models.BooleanField(default=False,
        verbose_name=u'Receptionist status',
        help_text='Designates whether this user should be  a hotel receptionist.')

    class Meta:
        db_table = 'auth_user'
