from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.http import HttpResponseRedirect
from django.conf import settings
admin.autodiscover()


urlpatterns = patterns('',
    url(r'^', include("booking.urls")),
    url(r'^api/', include("api.urls")),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^foundation/', include("foundation.urls")),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^socketio/$', lambda x: HttpResponseRedirect('http://localhost:7000')),
    )
