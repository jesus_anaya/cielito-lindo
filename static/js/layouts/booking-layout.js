define([
    'app',
    'marionette',
    'tpl!templates/booking-layout.html'
],
function(
    App,
    Marionette,
    BookingLayoutTemplate)
{
     var BookingLayout = Backbone.Marionette.Layout.extend({
        template: BookingLayoutTemplate,

        regions: {
            content: "#booking-content",
            users: "#users-list-container"
        },

        ui: {
            calendar: "#datepicker",
            room_filter: "#room-filter",
            grid_view: "#grid-view",
            refresh: "#refresh-btn"
        },

        events: {
            "change #room-filter": "changeFilter",
            "change #grid-view": "changeGridView",
            "click #refresh-btn": "refresh",
            "click #search-btn": "showSearch"
        },

        changeFilter: function(event) {
            App.vent.trigger('layout:change:filter', event);
        },

        changeGridView: function(event) {
            App.vent.trigger('layout:change:gridview', event);
        },

        refresh: function(event) {
            App.vent.trigger('layout:click:refresh', event);
        },

        showSearch: function() {
            App.vent.trigger('layout:show:search', event);
        }
    });
    return BookingLayout;
});
