function zeroFill(number, width) {
    width -= number.toString().length;
    if (width > 0) {
        return new Array(width + (/\./.test(number) ? 2 : 1)).join('0') + number;
    }
    return number.toString();
}

function dateToString(date) {
    return moment(date).format("YYYY-MM-DD");
}

function stringToDate(date_text) {
    return moment(date_text)._d;
}

function deltaTime(date, days) {
    var new_date = new Date(date);
    new_date.setDate(new_date.getDate() + days);
    return dateToString(new_date);
}

function dateFormat(current_date, days) {
    var date = new stringToDate(current_date);
    date.setDate(date.getDate() + days);
    return dateToString(date);
}
