
define([
    'app',
    'marionette',
    'views/users-connected',
    'views/rooms-view',
    'views/menu-save-view',
    'views/menu-modify-view',
    'views/reservation-form',
    'views/search',
    'collections/rooms',
    'layouts/booking-layout'],
function(
    App,
    Marionette,
    UsersConnected,
    RoomsView,
    MenuSaveView,
    MenuModifyView,
    ModalFormView,
    SearchView,
    Rooms,
    Layout)
{
    var Controller = Backbone.Marionette.Controller.extend({
        initialize: function(options) {
            this.maincontainer = options.maincontainer;

            // init instances
            this.initInstances();

            // init event handlers
            this.initEventHandlers();
        },

        initInstances: function() {
            this.layout = new Layout();
            this.rooms = new Rooms();
            this.current_date = new Date();
            this.column_days = 7; // default use 7 days
        },

        initEventHandlers: function() {
            // bind all
            _.bindAll(this, 'dateSelected');
            _.bindAll(this, 'renderRooms');

            this.listenTo(this.layout, 'render', this.layoutRender);
            this.listenTo(App.vent, 'window:mousedown', this.cancelSelections);
            this.listenTo(App.vent, 'layout:change:filter', this.changeRoomFilter);
            this.listenTo(App.vent, 'layout:change:gridview', this.changeGridView);
            this.listenTo(App.vent, 'layout:click:refresh', this.refresh);
            this.listenTo(App.vent, 'layout:show:search', this.showSearch);

            this.listenTo(App.vent, 'open:menu:create', this.openMenuCreate);
            this.listenTo(App.vent, 'open:form:modal', this.openFormModal);
            this.listenTo(App.vent, 'reservation:change', this.refresh);

            // results
            this.listenTo(App.vent, 'result:selected', this.openFormModal);

            // menu modify
            this.listenTo(App.vent, 'open:menu:modify', this.openMenuModify);
        },

        layoutRender: function(){
            this.layout.ui.calendar.datepicker({
                onSelect: this.dateSelected,
                dateFormat: "yy-mm-dd"
            });

            this.layout.users.show(new UsersConnected());
            this.getRows();
        },

        getRows: function() {
            this.rooms.fetch().done(this.renderRooms);
        },

        renderRooms: function() {
            this.layout.content.show(new RoomsView({
                collection: this.rooms,
                current_date: this.current_date,
                column_days: this.column_days
            }));
        },

        // Events Handlers
        openMenuCreate: function(information) {
            App.menu.show(new MenuSaveView({
                information: information
            }));
        },

        openMenuModify: function(information) {
            App.menu.show(new MenuModifyView(information));
        },

        openFormModal: function(model) {
            App.menu.show(new ModalFormView({
                model: model
            }));
        },

        cancelSelections: function() {
            if (App.menu) {
                App.menu.close();
            }
        },

        dateSelected: function(date_text) {
            this.current_date = moment(date_text)._d;
            this.getRows();
        },

        changeRoomFilter: function(event) {
            var value = $(event.target).val();
            var params = {};

            if(value !== "") {
                params = {'room_type': value};
            }
            this.rooms.fetch({data: $.param(params)});
        },

        changeGridView: function(event) {
            this.column_days = parseInt($(event.target).val(), 10);
            this.getRows();
        },

        refresh: function(event) {
            this.getRows();
        },

        showSearch: function(event) {
            App.menu.show(new SearchView());
        },

        // resultSelected: function(model) {
        //     App.menu.close();
        //     var show = App.menu.show;

        //     setTimeout(function() {
        //         show(new ModalFormView({
        //             model: model
        //         }));
        //     },
        //     1000);
        // },

        // render methods
        show: function() {
            this.maincontainer.show(this.layout);
        }

    });
    return Controller;
});
