define(['backbone', 'models/room'],
function(Backbone, Room) {
    var Rooms = Backbone.Collection.extend({
        url: '/api/rooms/',
        model: Room,

        events: {
            'reset:rooms': 'fetch'
        }
    });
    return Rooms;
});
