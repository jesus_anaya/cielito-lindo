
define(['backbone', 'models/reservation'],
function(Backbone, Reservation) {
    var Reservations = Backbone.Collection.extend({
        url: '/api/reservations/',
        model: Reservation,

        getFiltered: function(date, room) {
            this.fetch({data: $.param({fdate: date, room: room})});
            return this;
        },

        getReservations: function(params) {

            this.fetch({
                data: $.param({
                    filter_date: params.start_date + "_" + params.end_date,
                    room: params.room,
                    id_exclude: params.id
                }),
                success: params.success_func
            });
            return this;
        }
    });
    return Reservations;
});
