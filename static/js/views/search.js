define([
    'app',
    'marionette',
    'collections/reservations',
    'views/search-results',
    'tpl!templates/search.html'],
function(
    App,
    Marionette,
    Reservations,
    SearchResults,
    SearchTemplate)
{
    var SearchView = Backbone.Marionette.ItemView.extend({
        template: SearchTemplate,
        idAttribute: "search-modal",
        className: "reveal-modal search-modal",

        ui: {
            form: '#search-form-modal',
            search: '#search',
            results: '#results-area'
        },

        events: {
            'closed': 'close',
            'click .close-reveal-modal': 'close',
            'close @ui.form': 'close',
            'submit form': 'onSubmit',
        },

        initialize: function(options) {
            this.collection = new Reservations();

            _.bindAll(this, 'renderResults');
        },

        onBeforeRender: function() {
            this.$el.foundation('reveal', 'open');
        },

        onSubmit: function(event) {
            event.stopPropagation();
            event.preventDefault();

            if(this.ui.search.val() !== "") {
                this.collection.fetch({
                    data: $.param({
                        search: this.ui.search.val()
                    })
                })
                .done(this.renderResults);
            }
        },

        renderResults: function() {
            if(this.collection.length > 0) {
                var results = new SearchResults({
                    collection: this.collection,
                    el: this.ui.results
                });
                results.render();
            } else {
                this.ui.results.text('No results');
            }
        },

        onBeforeClose: function() {
            this.$el.foundation('reveal', 'close');
        },
    });

    return SearchView;
});
