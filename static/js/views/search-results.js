//========== Item Views =============//

define([
    'app',
    'marionette',
    'views/result-view',
    'tpl!templates/search-results.html'],
function(
    App,
    Marionette,
    ResultView,
    SearchResultsTemplate)
{
    var SearchResults = Backbone.Marionette.CompositeView.extend({
        itemView: ResultView,
        template: SearchResultsTemplate,
        itemViewContainer: 'tbody',
    });
    return SearchResults;
});
