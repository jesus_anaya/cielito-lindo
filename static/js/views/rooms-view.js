
define([
    'app',
    'marionette',
    'views/room-view',
    'tpl!templates/table-rooms.html'],
function(
    App,
    Marionette,
    RoomView,
    TemplateTableRooms)
{
    var RoomsView = Backbone.Marionette.CompositeView.extend({
        itemView: RoomView,
        itemViewContainer: "#tbody",
        template: TemplateTableRooms,

        initialize: function(options) {
            this.column_days = options.column_days;
            this.current_date = options.current_date;

            this.listenTo(App.vent, 'room:updated', this.render);
        },

        itemViewOptions: function() {
            return {
                column_days: this.column_days,
                current_date: this.current_date
            };
        },

        serializeData: function() {
            return this.itemViewOptions();
        },

        templateHelpers: {
            dayName: function(num_days) {
                var days_list = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
                var date = new Date(this.current_date);
                date.setDate(date.getDate() + num_days);
                return  date.getDate() + "<br>" + days_list[date.getDay()];
            },

            range: function(limit) {
                var array = [];
                for(var x = 0; x < limit; x++) {
                    array.push(x);
                }
                return array;
            }
        }
    });
    return RoomsView;
});
