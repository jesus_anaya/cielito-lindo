/***************************************************/

define([
    'app',
    'marionette',
    'tpl!templates/reservation-form.html'],
function(
    App,
    Marionette,
    ReservationFormTemplate)
{
    var ModalFormView = Backbone.Marionette.ItemView.extend({

        idAttribute: "add-booking",
        className: "reveal-modal",
        template: ReservationFormTemplate,

        ui: {
            form: '#form-modal',
            user_name: "#user-name",
            room_name: "#room-name",
            user_data: ".user-data",
            user_field: "#user-field",
            start_date: "#start_date",
            end_date: "#end_date",
            birthday: "#birthday",
            guest_color: "#guest_color",
            clinic_selection: "#clinic-selection",
            confirmed: "#confirmed",
        },

        events: {
            'hidden': 'close',
            'click .close-reveal-modal': 'close',
            'click #close-modal-button': 'close',
            'click #save-and-confirm': 'saveAndConfirm',
            'change #guest_color': 'isClinicGuest',
            'close @ui.form': 'close',
            'submit form': 'onSubmit'
        },

        saveAndConfirm: function(e) {
            this.ui.confirmed.val(true).change();
        },

        isClinicGuest: function(e) {
            if(this.ui.guest_color.val() === "0" /* Clinic guest */) {
                this.ui.clinic_selection.show();
            } else {
                this.ui.clinic_selection.hide();
            }
        },

        successMessage: function() {
            alert("The confirmation has been successfully sent.");
        },

        errorMessage: function() {
            alert("Error send the information.");
        },

        onBeforeRender: function() {
            // Open reveal
            this.$el.foundation('reveal', 'open');
        },

        onRender: function() {
            // // Start to listen modifications in the form
            this.ui.form.listenModifications();

            // // clear form
            // this.ui.form.resetForm();

            // // Validator config
            // this.ui.form.validate({
            //     rules: {
            //         email: 'email',
            //         price: 'number',
            //         free_days: 'number'
            //     }
            // });

            // date pickers
            this.ui.start_date.datepicker({
                dateFormat: "yy-mm-dd"
            });

            this.ui.end_date.datepicker({
                dateFormat: "yy-mm-dd"
            });

            // Birthday date picker
            this.ui.birthday.datepicker({
                dateFormat: "yy-mm-dd",
                yearRange: "-100:+0",
                changeMonth: true,
                changeYear: true
            });

            // set room name
            this.ui.room_name.html(this.model.get("room_name"));

            if(this.model.get("user_name")) {
                this.ui.user_name.html(this.model.get("user_name"));
            } else {
                this.ui.user_field.remove();
            }

            this.ui.form.parseForm(this.model.toJSON());

            // // validate user
            // this.check_user();

            // // validate clinic guest
            this.isClinicGuest();
        },

        onSubmit: function(event) {
            event.stopPropagation();
            event.preventDefault();

            this.model.set(this.ui.form.serializeFormJSON());
            // console.log("Confirmed: ", this.ui.confirmed.val());

            if(this.model.isNew()) {
                if (this.ui.confirmed.val() === "true") {
                    this.model.save(null, {
                        wait: true,
                        success: this.successMessage,
                        error: this.errorMessage
                    });
                } else {
                    this.model.save(null, {
                        wait: true
                    });
                }
                App.vent.trigger('render:row:' + this.model.get('room'));

            } else {
                if (this.ui.confirmed.val() === "true") {
                    this.model.patch(this.ui.form.data_modified, this.successMessage, this.errorMessage);
                } else {
                    this.model.patch(this.ui.form.data_modified);
                }
                App.vent.trigger('render:reservation:' + this.model.get('id'));
            }
            this.close();
        },

        onBeforeClose: function() {
            this.$el.foundation('reveal', 'close');
        },

        check_user: function() {
            var user = $("meta[name='user-id']").attr("content");
            var root = $("meta[name='user-root']").attr("content");

            // if(this.model.get('user') &&
            //     this.model.get('user') !== parseInt(user, 10) &&
            //     root !== "true")
            // {
            //     this.ui.user_data.remove();
            // }
        }
    });
    return ModalFormView;
});
