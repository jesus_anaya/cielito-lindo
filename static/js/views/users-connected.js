
define([
    'app',
    'marionette',
    'tpl!templates/users-connected.html',
    'socketio'],
function(
    App,
    Marionette,
    TemplateUsersConnected)
{
    var server_url = 'http://sys.haciendalosalgodones.com:7000';

    var UsersConnected = Backbone.Marionette.ItemView.extend({
        template: TemplateUsersConnected,
        className: 'users-list',

        socket_events: {
            'update_changes': 'updateChanges',
            'connected': 'connected',
            'new-user': 'newUser'
        },

        initialize: function(options) {
            this.users = [];
            this.socket = io.connect(server_url);

            if (this.socket_events && _.size(this.socket_events) > 0) {
                this.delegateSocketEvents(this.socket_events);
            }
        },

        serializeData: function() {
            return {
                users: this.users
            };
        },

        delegateSocketEvents: function(events) {
            for (var key in events) {
                var method = events[key];
                if (!_.isFunction(method)) {
                    method = this[events[key]];
                }

                if (!method) {
                    throw new Error('Method "' + events[key] + '" does not exist');
                }

                method = _.bind(method, this);
                this.socket.on(key, method);
            }
        },

        // Event methods
        updateChanges: function(data) {
            App.vent.trigger('users:update');
        },

        connected: function(data) {
            var user_name = $('meta[name="user-name"]').attr('content');
            this.socket.emit("user-name", {id: data, name: user_name});
        },

        newUser: function(users) {
            this.users = users;
            this.render();
        }
    });
    return UsersConnected;
});
