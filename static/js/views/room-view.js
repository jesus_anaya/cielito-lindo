define([
    'app',
    'marionette',
    'collections/reservations',
    'views/reservation-view',
    'tpl!templates/room-row.html'],
function(
    App,
    Marionette,
    Reservations,
    ReservationView,
    RoomRowTemplate)
{
    var counter = 0;

    var RoomView = Backbone.Marionette.CompositeView.extend({
        events: {
            'mousedown .room': 'mouseDown',
            'mousemove .room': 'mouseMove',
            'mouseup': 'mouseUp',
            'mouseleave': 'mouseUp',
            'click .room': 'preventClick'
        },
        tagName: 'tr',
        template: RoomRowTemplate,
        itemViewContainer: ".first",
        itemView: ReservationView,

        initialize: function(options) {
            this.column_days = options.column_days;
            this.current_date = options.current_date;

            this.mouse = {
                is_drag: false,
                start_posx: 0,
                start_posy: 0,
                move_to: ''
            };

            this.collection = new Reservations();

            // listen app events
            this.listenTo(App.vent, 'menu:close', this.clear);
            this.listenTo(App.vent, 'render:row:' + this.model.get('id'), this.render);

            this.collection.fetch({
                data: $.param({
                    room: this.model.get('id'),
                    edate: dateToString(this.current_date),
                    sdate: deltaTime(this.current_date, this.column_days)
                })
            });
        },

        itemViewOptions: function() {
            return {
                column_days: this.column_days
            };
        },

        serializeData: function() {
            var data = this.model.toJSON();
            data.column_days = this.column_days;
            data.current_date = this.current_date;

            return data;
        },

        refresh: function() {
            App.vent.trigger('room:updated');
        },

        mouseDown: function(event) {
            this.mouse.is_drag = true;
            this.mouse.start_posx = event.pageX;
            this.mouse.start_posy = event.pageY;
        },

        mouseUp: function(event) {
            if (this.mouse.is_drag) {

                this.mouse.is_drag = false;
                this.mouse.move_to = '';

                var selecteds = this.$el.find('.selected');

                var first = selecteds.first().attr('data-date');
                var last = selecteds.last().attr('data-date');

                if (first !== last) {
                    var data = {
                        room: this.model.get('id'),
                        room_name: this.model.get('room_type_name'),
                        start_date: first,
                        end_date: last,
                        position: {
                            x: event.pageX,
                            y: event.pageY
                        }
                    };

                    App.vent.trigger('open:menu:create', data);
                }
            }
        },

        clear: function(event) {
            this.rooms.removeClass('selected');
        },

        mouseMove: function(event) {
            if(this.mouse.is_drag) {
                if(this.mouse.start_posx !== event.pageX) {
                    if(this.mouse.start_posx > event.pageX) {
                        this.mouse.move_to = 'left';
                    } else {
                        this.mouse.move_to = 'right';
                    }
                }

                switch(this.mouse.move_to) {
                    case 'left':
                        this.calcSelection(event.pageX, this.mouse.start_posx);
                        break;
                    case 'right':
                        this.calcSelection(this.mouse.start_posx, event.pageX);
                        break;
                }
            }
        },

        calcSelection: function(pos_x1, pos_x2) {
            var diff = pos_x1 - pos_x2;
            this.$el.find('.room').each(function(index, value) {
                var right = $(value).offset().left + $(value).width();
                var left = $(value).offset().left;

                if(right >= pos_x1 && right <= pos_x2 ||
                    (left <= pos_x2 && right >= pos_x2))
                {
                    $(value).addClass('selected');
                } else {
                    $(value).removeClass('selected');
                }
            });
        },

        onRender: function() {
            this.rooms = this.$el.find('.room');
        },

        preventClick: function(e) {
            e.stopPropagation();
        },

        templateHelpers: {
            getDate: function(days) {
                return deltaTime(this.current_date, days);
            },

            range: function(limit) {
                var array = [];
                for(var x = 0; x < limit; x++) {
                    array.push(x);
                }
                return array;
            },

            getClass: function() {
                if(this.column_days === 30) {
                    return 'small-col';
                } else {
                    return 'long-col';
                }
            }
        },

        // // The default implementation:
        // appendHtml: function(collectionView, itemView, index){
        //     if (collectionView.isBuffering) {
        //         // buffering happens on reset events and initial renders
        //         // in order to reduce the number of inserts into the
        //         // document, which are expensive.
        //         collectionView.elBuffer.appendChild(itemView.el);
        //     }
        //     else {
        //         // If we've already rendered the main collection, just
        //         // append the new items directly into the element.
        //         collectionView.$el.append(itemView.el);
        //     }
        // },

        // // Called after all children have been appended into the elBuffer
        // appendBuffer: function(collectionView, buffer) {
        //     collectionView.$el.append(buffer);
        // },

        // // called on initialize and after appendBuffer is called
        // initRenderBuffer: function() {
        //      this.elBuffer = document.createDocumentFragment();
        // }

    });
    return RoomView;
});
