define([
    'app',
    'marionette',
    'tpl!templates/drop-down-menu.html'],
function(
    App,
    Marionette,
    DropDownMenuTemplate)
{
    var MenuModifyView = Backbone.Marionette.ItemView.extend({
        template: DropDownMenuTemplate,
        className: 'menu-modify-booking',

        events: {
            'click #menu-view-info': 'viewInfo',
            'click #menu-block': 'menuBlock',
            'click #menu-change': 'menuChange',
            'click #menu-no-paid': 'menuNoPaid',
            'click #menu-delete': 'menuDelete',
            'click #menu-cancel': 'menuCancel'
        },

        initialize: function(options) {
            this.$el.css({
                top: options.position.y,
                left: options.position.x
            });
        },

        serializeData: function() {
            return {
                rows: [
                    'view-info',
                    'block',
                    'no-paid',
                    'delete',
                    'cancel'
                ]
            };
        },

        viewInfo: function() {
            this.closeMenu();
            App.vent.trigger('open:form:modal', this.model);
        },

        menuDelete: function() {
            this.closeMenu();
            if(confirm("Are you sure that you want to delete this booking")) {
                this.model.remove();
            }
        },

        menuBlock: function() {
            var blocked_days = prompt("days blocked?", this.model.get('blocked_days'));
            if(blocked_days && blocked_days !== "") {
                this.model.changeBlockedDays(blocked_days);
                App.vent.trigger('render:reservation:' + this.model.get('id'));
            }
            this.closeMenu();
        },

        menuNoPaid: function() {
            var no_paid_days = prompt("change days", this.model.get('no_paid_days'));
            if(no_paid_days && no_paid_days !== "") {
                this.model.patch({'no_paid_days': no_paid_days});
                App.vent.trigger('render:reservation:' + this.model.get('id'));
            }
            this.closeMenu();
        },

        menuCancel: function() {
            this.closeMenu();
        },

        closeMenu: function() {
            this.close();
        },
    });

    return MenuModifyView;
});
