//========== Item Views =============//

define([
    'app',
    'marionette',
    'models/reservation',
    'tpl!templates/drop-down-menu.html'],
function(
    App,
    Marionette,
    Reservation,
    DropDownMenuTemplate)
{
    var MenuSaveView = Backbone.Marionette.ItemView.extend({
        template: DropDownMenuTemplate,
        className: 'menu-save-booking',

        events: {
            'click #menu-save': 'onMenuSave',
            'click #menu-close': 'onMenuClose'
        },

        initialize: function(options) {
            this.information = options.information;
        },

        serializeData: function() {
            return {
                rows: ['save', 'close']
            };
        },

        // events handlers
        onMenuSave: function(event) {
            var reservation = new Reservation({
                room: this.information.room,
                room_name: this.information.room_name,
                start_date: this.information.start_date,
                end_date: this.information.end_date
            });

            App.vent.trigger('open:form:modal', reservation);
            this.close();
        },

        onMenuClose: function(event) {
            this.close();
        },

        onRender: function() {
            this.$el.offset({
                top: this.information.position.y,
                left: this.information.position.x
            });
        },

        onClose: function() {
            App.vent.trigger('menu:close');
        }
    });
    return MenuSaveView;
});
