define([
    'app',
    'marionette',
    'tpl!templates/result-view.html'],
function(
    App,
    Marionette,
    ResultViewTemplate)
{
    var ResultView = Backbone.Marionette.ItemView.extend({
        tagName: 'tr',
        template: ResultViewTemplate,

        events: {
            'click .name': 'select'
        },

        select: function() {
            App.vent.trigger('result:selected', this.model);
        }
    });
    return ResultView;
});
