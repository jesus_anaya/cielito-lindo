/*********************************************************/
/*********** ReservationView *****************************/
/*********************************************************/

define([
    'app',
    'marionette',
    'collections/reservations',
    'tpl!templates/reservation.html'],
function(
    App,
    Marionette,
    Reservations,
    ReservationTemplate)
{
    var ReservationView = Backbone.Marionette.ItemView.extend({
        tagName: 'div',
        className: 'reservation-cell',
        template: ReservationTemplate,

        events: {
            'click td': 'clickCell',
            'click .user-name-container': 'clickCell',
            'mousedown .right-pointer': 'rightPointer',
            'mousedown .left-pointer': 'leftPointer',
        },

        initialize: function(options) {
            window.socket = io.connect('http://sys.haciendalosalgodones.com:7000');
            this.column_days = options.column_days;

            // initialize room width
            switch(options.column_days) {
                case App.MONTHLY:
                    this.room_width = 27; // width pixels
                    break;

                case App.WEEKLY:
                    this.room_width = 115; // width pixels
                    break;
            }

            _.bindAll(this, 'fetch');
            _.bindAll(this, 'postModify');

            this.listenTo(App.vent, 'render:reservation:' + this.model.get('id'), this.fetch);
            this.listenTo(this.model, 'change', this.render);
        },

        serializeData: function() {
            var data = {
                column_days: this.column_days,
                start_moved: this.start_moved,
                end_moved: this.end_moved
            };

            return _.extend(data, this.model.toJSON());
        },

        fetch: function() {
            this.model.fetch();
        },

        collisionValidation: function() {
            var params = {
                start_date: this.model.get('start_date'),
                end_date: this.model.get('end_date'),
                room: this.model.get('room'),
                id: this.model.get('id'),
                success_func: this.postModify
            };

            this.reservation = new Reservations().getReservations(params);
        },

        postModify: function() {
            if (this.reservation.length > 0) {
                alert("You can't do this operation!");
                App.vent.trigger('reservation:change');
            } else {
                if(confirm("Are you sure that you want to modify this booking?")) {
                    this.model.updateModifiedData();
                } else {
                    App.vent.trigger('reservation:change');
                }
            }
        },

        leftPointer: function() {
            var self = this;
            var start_x = 0;
            var start_y = 0;
            var last_counter_x = 0;
            var last_counter_y = 0;
            var original_pos_y = this.$el.position().top;
            var start_date = self.model.get('start_date');
            var end_date = self.model.get('end_date');

            $(document).on("mousemove", function(e){
                var pos_moved_x = 0;
                var pos_moved_y = 0;

                if(start_x === 0 || start_y === 0) {
                    start_x = e.pageX;
                    start_y = e.pageY;
                }

                pos_moved_x = parseInt((e.pageX - start_x) / self.room_width, 10);
                pos_moved_y = parseInt((e.pageY - start_y) / 27, 10);

                if(pos_moved_x !== last_counter_x) {
                    self.model.fset({
                        'start_date': dateFormat(start_date, pos_moved_x),
                        'end_date': dateFormat(end_date, pos_moved_x)
                    });

                    self.render();
                }

                if(pos_moved_y !== last_counter_y) {
                    var position = original_pos_y + (pos_moved_y * 27);

                    $("#rooms-table").children("tbody").children("tr").each(function() {
                        var td = $(this).children("td").get(3);

                        if($(td).position().top + 1 === position) {
                            var room = $(td).attr('data-room');

                            self.model.fset({'room': room});
                            self.render();
                        }
                    });
                }

                last_counter_x = pos_moved_x;
                last_counter_y = pos_moved_y;
            });

            $(document).on("mouseup", function(e){
                $(document).off("mousemove mouseup");
                self.collisionValidation();
            });
        },

        rightPointer: function(e) {
            var self = this;
            var start = 0;
            var last_counter = 0;
            var original_nights = self.model.get('nights');
            var start_date = self.model.get('start_date');

            $(document).on("mousemove", function(e){
                if(start === 0) {
                    start = e.pageX;
                }
                var pos_moved = parseInt((e.pageX - start) / self.room_width, 10);

                if(pos_moved !== last_counter) {
                    var new_nights = original_nights + pos_moved;

                    self.model.fset({
                        'end_date': dateFormat(self.model.get('start_date'), new_nights),
                        'nights': new_nights
                    });
                    self.render();
                }
                last_counter = pos_moved;
            });

            $(document).on("mouseup", function(e){
                $(document).off("mousemove mouseup");
                self.collisionValidation();
            });
        },

        clickCell: function(event) {
            event.stopPropagation();

            App.vent.trigger('open:menu:modify', {
                model: this.model,
                position: {
                    x: event.pageX,
                    y: event.pageY
                }
            });
        },

        setPosition: function(position) {
            this.position = position;
        },

        templateHelpers: {
            range: function(limit) {
                var array = [];
                for(var x = 0; x < limit; x++) {
                    array.push(x);
                }
                return array;
            }
        },

        onBeforeRender: function() {

            /* Precalculate the reservation start position */
            var td_tag = null;
            var original_date = this.model.get('start_date');
            var final_date = this.model.get('end_date');
            var start_date = original_date;
            var end_date = final_date;

            var room = this.model.get('room');
            var num_nights = this.model.get('nights');
            var less_nights = 0;
            var reservation_size = 0;

            /* move position if the start_date is not in the table, find start position */
            for(var x = 0; x < num_nights; x++) {
                td_tag = $('#' + room + '-' + start_date);
                if (td_tag.length > 0) {
                    break;
                }
                this.start_moved = true;
                start_date = dateFormat(original_date, x + 1);
                var diff =  moment(start_date)._d - moment(original_date)._d;
                less_nights = parseInt(diff / 1000 / 60 / 60 / 24); // millis / seconds / minutes / hours
            }
            console.log(original_date, start_date, less_nights, end_date, final_date, num_nights);

            /* recalculate the number of day starting of the start_date */
            num_nights -= less_nights;

           /* set nights to models */
           this.model.set('visible_nights', num_nights);

            /* validate that num_nights can't be less that 0 */
            var zero_nights = false;
            if(num_nights <= 0) {
                num_nights = 1;
                zero_nights = true;
            }

            /* claculate end of reservation item */
            less_nights = 0;
            for(x = 0; x < num_nights; x++) {
                var end_tag = $('#' + room + '-' + end_date);
                if (end_tag.length > 0) {
                    break;
                }
                this.end_moved = true;
                less_nights--;
                end_date = dateFormat(final_date, (-x) - 1);
            }

            /*  recailculate eservation size */
            if(less_nights != 0) {
                reservation_size = ((num_nights + less_nights) * this.room_width) + (this.room_width / 2) - 1;
            } else {
                reservation_size = (num_nights * this.room_width) - 1;
            }

            /* render the reservation rectangle */
            if (td_tag) {
                var positions = {};

                if (td_tag.position()) {
                    positions = {
                        width: reservation_size,
                        top: td_tag.position().top + 1,
                        left: td_tag.position().left + (this.room_width / 2) + 1
                    };

                    // validate if start date was moved
                    if (this.start_moved) {
                       positions.width += (this.room_width / 2);
                       positions.left -= (this.room_width / 2);
                    }
                    console.log(":0");

                } else {
                    var first_tag = $('td[data-room="' + room + '"]').first();
                    console.log("td_tag", td_tag, "room ", room);

                    positions = {
                        width: (this.room_width / 2),
                        top: first_tag.position().top + 1,
                        left: first_tag.position().left + 1
                    };
                }
                this.$el.css(positions);
            }
        },

        onRender: function() {
            var colors = [
                "color-light-green",
                "color-green",
                "color-yellow",
                "color-blue",
                "color-red",
                "color-brown",
                "color-booking",
                "color-expidia",
            ];

            this.$el.find('.user-name').addClass(colors[this.model.get('color')]);
            //this.$el.find("table").css('width', ((this.model.get('nights') * this.room_width) - 2) + 'px');

            var cells = this.$el.find('td');
            var blocked_days = parseInt(this.model.get('blocked_days'), 10);
            var no_paid_days = parseInt(this.model.get('no_paid_days'), 10);
            var num = blocked_days + no_paid_days;

            if(num > 0) {
                if(this.start_moved){
                    num ++;
                }

                if(this.end_moved){
                    num ++;
                }

                for (var x = 0; x < num; x++) {
                    if (x < blocked_days && blocked_days > 0) {
                        $(cells[cells.length - x - 1]).addClass("color-red");
                    } else if (x - blocked_days <= no_paid_days && no_paid_days > 0) {
                        $(cells[cells.length - x - 1]).addClass("color-purple");
                    }
                }
            }
        }
    });
    return ReservationView;
});
