require.config({
    baseUrl: '/static/js/',
    paths: {
        backbone: 'libs/backbone-min',
        underscore: 'libs/underscore-min',
        marionette: 'libs/backbone.marionette.min',
        courier: 'libs/backbone.courier.min',
        socketio: 'libs/socket.io',
        templates: '../templates'
    },
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ["underscore"],
            exports: "Backbone"
        },
        courier: {
            deps: ["backbone"]
        },
        marionette: {
            deps: ["backbone", "courier"],
            exports:"Marionette"
        },
        common: {
            deps:["marionette"]
        }
    },
    //urlArgs: "bust=" + (new Date()).getTime(),
});

define(['app', 'controllers/app-controller'], function(App, Controller) {
    var _sync = Backbone.sync;
    Backbone.sync = function(method, model, options){
        options.beforeSend = function(xhr){
            var token = $('#csrf-token').attr('content');
            xhr.setRequestHeader('X-CSRFToken', token);
        };
        return _sync(method, model, options);
    };

    $(document).foundation('reveal', {animation: 'none'});

    // Init application configurations and start
    App.addRegions({
        maincontainer: "#main",
        menu: "#menu-container"
    });

    App.on('initialize:before', function(options) {
        // listen window events
        $(document).on('mousedown', function(event) {
            //App.vent.trigger('window:mousedown', event);
        });

        // instance application controller
        var controller = new Controller({
            maincontainer: App.maincontainer
        });
        controller.show();
    });

    return App.start();
});
