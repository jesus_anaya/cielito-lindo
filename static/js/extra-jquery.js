
// estra jquery functions
$.fn.serializeFormJSON = function() {
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {

        if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
    });
    $(this).find("input:checkbox").each(function(){
        o[this.name] = this.checked;
    });
   return o;
};

$.fn.parseForm = function(data) {
    var self = this;

    $.each(data, function(name, value) {
      var input = self.find("input[name='" + name + "']");
      if(input.length === 0) {
        input = self.find("select[name='" + name + "']");
        if(input.length === 0) {
          input = self.find("textarea[name='" + name + "']");
          if(input.length === 0) return;
        }
      }

      switch(input.attr("type")) {
        case "text":
          input.val(value);
          break;

        case "checkbox":
          input.attr("checked", value);
          break;

        default:
          input.val(value);
          break;
      }

    });
};

$.fn.listenModifications = function() {
  $.fn.data_modified = {};
  var data = this.data_modified;

  this.find('input').on('change', function(){
    if($(this).attr('type') === "checkbox") {
      data[$(this).attr('name')] = $(this).is(':checked');
    } else {
      data[$(this).attr('name')] = $(this).val();
    }
  });

  this.find('select').on('change', function(){
    data[$(this).attr('name')] = $(this).val();
  });

  this.find('textarea').on('change', function(){
    data[$(this).attr('name')] = $(this).val();
  });
};

