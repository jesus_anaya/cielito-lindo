define(['backbone'],
function(Backbone){
    var Room = Backbone.Model.extend({
        urlRoot: '/api/rooms/',
        idAttribute: '_id'
    });

    return Room;
});
