define(['backbone'],
function(Backbone) {
    var Reservation = Backbone.Model.extend({
        urlRoot : '/api/reservations/',
        idAttribute: '_id',

        initialize: function() {
            this.on('sync', this.sync_update);
            this.modified = {};

            _.bindAll(this, 'postModify');

            //this.reservation = new Reservations();
        },

        collisionValidation: function(end_date) {

            // var params = {
            //     start_date: this.get('start_date'),
            //     end_date: end_date,
            //     room: this.get('room'),
            //     id: this.get('id'),
            //     success_func: this.postModify
            // };

            // this.reservation.getReservations(params);

            var params = $.param({
                filter_date: this.get('start_date') + "_" + end_date,
                room: this.get('room'),
                id_exclude: this.get('id')
            });

            $.getJSON( "/api/reservations/.json?" + params, this.postModify);
        },

        postModify: function(data) {
            if (data.length > 0) {
                alert("You can't do this operation!");
                this.fetch();
                this.flush();
            } else {
                this.updateModifiedData();
            }
        },

        sync_update: function(e) {
            try {
                window.socket.emit("new_change", window.socket_id);
            } catch(err) {
                console.log("Error open Socket.io: ", err.toString());
            }
            console.log("Reservation sync called: ", this.get('name') + ' ' + this.get('id'));
        },

        fset: function(data) {
            this.modified = $.extend(this.modified, data);
            this.set(data);
        },

        flush: function() {
            this.modified = {};
        },

        updateModifiedData: function() {
            this.patch(this.modified);
            this.modified = {};
        },

        update: function() {
            this.set('_id', this.get('id'));
            this.save({}, {
                wait: true,
            });
        },

        patch: function(data, success, error) {
            data = (data === undefined ? {} : data);
            success = (success === undefined ? function(){} : success);
            error = (error === undefined ? function(){} : error);

            this.set('_id', this.get('id'));
            this.save(data, {
                wait: true,
                patch: true,
                merge: true,
                success: success,
                error: error
            });
        },

        changeBlockedDays: function(blocked_days) {
            var days = parseInt(blocked_days, 10) - this.get('blocked_days');
            //var date = dateFormat(this.get('end_date'), days);
            var date = moment(this.get('end_date')).add(days, 'days').format("YYYY-MM-DD");
            console.log(date);

            this.fset({
                blocked_days: blocked_days,
                end_date: date
            });

            this.collisionValidation(date);
        },

        remove: function() {
            this.set('_id', this.get('id'));
            this.destroy();
        },

        isNew: function() {
            return this.get('id') === undefined;
        }
    });
    return Reservation;
});
