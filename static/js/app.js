
define(['marionette'],
function(Marionette) {
    var App = new Backbone.Marionette.Application();

    // Application constants
    App.MONTHLY = 30;
    App.WEEKLY = 7;

    return App;
});

//========== Initialize application ==========//
// function initializeJS() {

//     // Validator config
//     $("#form-modal").validate({
//         rules: {
//             email: 'email',
//             price: 'number',
//             free_days: 'number'
//         }
//     });

//     // Refresh button method
//     $("#refresh-btn").on('click', function(){
//         $('meta[name="filter-data"]').change();
//     });

//     // Filter date
//     $("#room-filter").on('change', function(){
//         var value =  $(this).find("option:selected").val();
//         $('meta[name="filter-data"]').attr('data-rooms', value).change();
//     });

//     // Socket.io Configuration
//     try {
//         window.socket = io.connect('http://sys.haciendalosalgodones.com:7000');

//         window.socket.on('update_changes', function (data) {
//             $('meta[name="filter-data"]').change();
//         });

//         window.socket.on('connected', function (data) {
//             var user_name = $('meta[name="user-name"]').attr('content');
//             window.socket_id = data;

//             window.socket.emit("user-name", {id: data, name: user_name});
//         });

//         window.socket.on('new-user', function (users) {
//             $(".users-list > .users").empty();
//             for(var index = 0; index < users.length; index++) {
//                 var user = users[index];
//                 $(".users-list > .users").append("<li>" + user + "</li>");
//             }
//         });

//     } catch(err) {
//         console.log("Error opening Socket.io: ", err.toString());
//     }
// }

// BookingApp.addInitializer(function(option) {
//     initializeJS();

//     // get room array
//     BookingApp.rooms = new Rooms();
//     BookingApp.rooms.fetch({
//         success: function() {
//             BookingApp.content.show(new RoomsView({
//                 collection: BookingApp.rooms
//             }));
//         }
//     });

//     $(document).on('click', function(){
//         // close all menu popups
//         BookingApp.menu.close();

//         // clear rows
//         $(".selected").removeClass("selected");
//     });

// });

// BookingApp.start();