var io = require('socket.io').listen(7000);

var sockets = {};

function users_list() {
    var names = [];
    for(var index in sockets) {
        names.push(sockets[index].name);
    }
    return names;
}

io.sockets.on('connection', function (socket) {

    sockets[socket.id] = socket;
    socket.emit('connected', socket.id);

    socket.on('new_change', function(data) {
        for(var index in sockets) {
            if(index !== data) {
                sockets[index].emit('update_changes', "new");
            }
        }
    });

    socket.on('user-name', function(data) {
        sockets[data.id].name = data.name;
        io.sockets.emit('new-user', users_list());
    });

    socket.on('disconnect', function() {
        delete sockets[socket.id];
        io.sockets.emit('new-user', users_list());
    });

});