from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings


NO_ACTIONS = 0
CONFIRMATION = 1
REMAINDER = 2
FEEDBACK = 3
ENDED = 4

def change_phase(etrack):
    if etrack.phase == CONFIRMATION:
        etrack.phase = REMAINDER
    elif etrack.phase == REMAINDER:
        etrack.phase = FEEDBACK
    elif etrack.phase == FEEDBACK:
        etrack.phase = ENDED
    etrack.save()


def send_email_template(subject, template, etrack):
    context = {'reservation': etrack.reservation}
    html_content = render_to_string(template, context)
    text_content = strip_tags(html_content)

    message = EmailMultiAlternatives(
        subject=subject,
        body=text_content,
        from_email="Cielito Lindo Hotel <cielitolindo@mediplaza.info>",
        to=[etrack.reservation.email],
        bcc=settings.BCC_EMAILS
    )
    message.attach_alternative(html_content, "text/html")
    message.send()

    change_phase(etrack)

