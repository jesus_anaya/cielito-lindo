from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from booking.models import Reservation
from .helpers import (send_email_template, NO_ACTIONS, CONFIRMATION,
                                        REMAINDER, FEEDBACK, ENDED)


PHASES = (
    (NO_ACTIONS, 'No actions'),
    (CONFIRMATION, 'Confirmation'),
    (REMAINDER, 'Remainder'),
    (FEEDBACK, 'Feedback'),
    (ENDED, 'Ended')
)

class EmailTrack(models.Model):
    class Meta:
        ordering = ('-date',)

    reservation = models.ForeignKey(Reservation)
    phase = models.IntegerField(default=0, choices=PHASES)
    date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def start_date(self):
        return self.reservation.start_date

    def end_date(self):
        return self.reservation.end_date


@receiver((post_save,), sender=Reservation)
def email_track(sender, **kwargs):
    instance = kwargs.get('instance')

    if instance and instance.confirmed:
        subject = "Booking Confirmation - Cielito Lindo"
        etrack, created = EmailTrack.objects.get_or_create(reservation=instance)

        if instance.clinic_guest:
            template = "emails/reservation_patient_guest.html"
        else:
            template = "emails/reservation_regular_guest.html"

        # Send confirmation email
        etrack.phase = CONFIRMATION
        send_email_template(subject, template, etrack)

        # Reset confirmation flag
        instance.confirmed = False
        instance.save()
    else:
        print "Send email not confirmed"


