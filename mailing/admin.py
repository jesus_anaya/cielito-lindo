from django.contrib import admin
from .models import EmailTrack


class EmailTrackAdmin(admin.ModelAdmin):
    list_display = ('reservation', 'phase', 'modified', 'date')


admin.site.register(EmailTrack, EmailTrackAdmin)
