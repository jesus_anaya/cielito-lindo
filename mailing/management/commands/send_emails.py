from django.core.management.base import BaseCommand
from mailing.models import EmailTrack
from mailing.helpers import send_email_template
from mailing.helpers import CONFIRMATION, REMAINDER, FEEDBACK
from datetime import timedelta, date
import logging
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    queryset = EmailTrack.objects.all()

    def confirmation(self, etrack):
        subject = "Reservation - Cielito Lindo"
        template = "emails/confirmation.html"
        self.send(subject, template, etrack)
        logging.info("send Reservation confirmation {0}".format(etrack.reservation))

    def remainder(self, etrack):
        subject = "Reminder Cielito Lindo"
        template = "emails/reminder.html"
        if date.today() >= etrack.start_date() - timedelta(days=3):
            if date.today() <= etrack.start_date() + timedelta(days=1):
                self.send(subject, template, etrack)
                logging.info("send Remainder email {0}".format(etrack.reservation))

    def feedback(self, etrack):
        subject = "Feedback Cielito Lindo"
        template = "emails/feedback.html"
        if date.today() >= etrack.end_date() + timedelta(days=1):
            if date.today() < etrack.end_date() + timedelta(days=5):
                self.send(subject, template, etrack)
                logging.info("send Remainder email {0}".format(etrack.reservation))

    def send(self, subject, template, etrack):
        send_email_template(subject, template, etrack)

    def handle(self, *args, **options):
        logging.info("Starting booking send emails")

        for etrack in self.queryset[:50]:
            if etrack.phase == CONFIRMATION:
                self.confirmation(etrack)
            elif etrack.phase == REMAINDER:
                self.remainder(etrack)
            elif etrack.phase == FEEDBACK:
                self.feedback(etrack)
