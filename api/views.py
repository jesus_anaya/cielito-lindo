from rest_framework.permissions import IsAuthenticated
from booking.models import Reservation, Room
from .permissions import IsOwner
from .serializers import ReservationSerializer, RoomSerializer
from .filters import ReservationFilter, RoomFilter
from .log_view import ModelViewSetLog


class ReservationViewSet(ModelViewSetLog):
    queryset = Reservation.objects.all()
    serializer_class = ReservationSerializer
    filter_class = ReservationFilter
    permission_classes = (IsAuthenticated,)
    model_log_name = "Reservation"

    def pre_save(self, obj):
        if not obj.id:
            obj.user = self.request.user


class RoomViewSet(ModelViewSetLog):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer
    filter_class = RoomFilter
    model_log_name = "Room"

