from django.conf.urls import patterns, url, include
from rest_framework import routers
from .views import ReservationViewSet, RoomViewSet

router = routers.DefaultRouter()
router.register(r'rooms', RoomViewSet)
router.register(r'reservations', ReservationViewSet)

urlpatterns = patterns('',
    url(r'^', include(router.urls)),
)
