from django.db.models import Q
from django_filters.filters import Filter, DateTimeFilter
from django_filters import FilterSet
from datetime import datetime
from datetime import datetime, timedelta
from booking.models import Reservation, Room


class ReservationDateFeld(Filter):
    """
    Filtering when
    """
    def filter(self, queryset, value):
        if value:
            current_date = datetime.strptime(value, "%Y-%m-%d")
            queryset = queryset.filter(
                end_date__gte=current_date,
                start_date__lte=current_date + timedelta(days=29))
        return queryset


class Search(Filter):
    def filter(self, queryset, value):
        if value:
            queryset = queryset.filter(Q(name__icontains=value) |
                Q(email__icontains=value) | Q(email__icontains=value))
        return queryset


class ReservationCollisionDateFeld(Filter):
    def filter(self, queryset, value):
        if value:
            try:
                sdate, edate = value.split('_')
                sdate = datetime.strptime(sdate, "%Y-%m-%d")
                edate = datetime.strptime(edate, "%Y-%m-%d")

                queryset1 = queryset.filter(
                    Q(start_date__lt=edate) & Q(start_date__gt=sdate))

                queryset2 = queryset.filter(
                    Q(end_date__lt=edate) & Q(end_date__gt=sdate))

                queryset = queryset1 | queryset2
            except ValueError:
                print '''Error in date filer format.
                        example:
                            filter_date=2012-12-12_2012-12-25'''
        return queryset


class ReservationExcluded(Filter):
    def filter(self, queryset, value):
        if value:
            queryset = queryset.exclude(id=int(value))
        return queryset


class ReservationFilter(FilterSet):
    sdate = DateTimeFilter(name='start_date', lookup_type='lt')
    edate = DateTimeFilter(name='end_date', lookup_type='gte')
    fdate = ReservationDateFeld()
    filter_date = ReservationCollisionDateFeld()
    id_exclude = ReservationExcluded()
    search = Search()

    class Meta:
        model = Reservation
        fields = (
            'fdate',
            'room',
            'filter_date',
            'id_exclude',
            'sdate',
            'edate',
            'search',
        )


class RoomFilter(FilterSet):
    class Meta:
        model = Room
        fields = ('room_type',)

