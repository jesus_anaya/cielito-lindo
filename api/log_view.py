from rest_framework import viewsets
from booking.models import Room, Log, Reservation, LOG_CREATED, LOG_MODIFIED, LOG_DELETED
import re

def get_id(path):
    results = re.findall("[0-9]+", path)
    if len(results) > 0:
        return results.pop()
    return 0


class ModelViewSetLog(viewsets.ModelViewSet):
    allowed_log_methods = ('post', 'put', 'patch', 'delete')
    model_log_name = ""

    def dict_to_string(self, obj):
        lines = []
        for key, value in obj.items():
            lines.append("%s:%s" % (key, value))
        return ",".join(lines)

    def create(self, request, *args, **kwargs):
        response = super(ModelViewSetLog, self).create(request, *args, **kwargs)

        if request.DATA.get('room'):
            request.DATA['room'] = Room.objects.get(id=int(request.DATA.get('room')))

        if response.status_code == 201 or response.status_code == 200:
            Log.objects.create(
                table=self.model_log_name,
                description=self.dict_to_string(request.DATA),
                user=request.user,
                type=LOG_CREATED
            )
        return response

    def put_log(self, request, status_code):
        pass

    def partial_update(self, request, *args, **kwargs):
        response = super(ModelViewSetLog, self).partial_update(request, *args, **kwargs)

        # Add reservation register to DATA
        request.DATA['reservation'] = Reservation.objects.get(id=int(get_id(request.path)))

        # Add room name to DATA by room id
        if request.DATA.get('room'):
            request.DATA.pop('room')

        if response.status_code == 200:
            Log.objects.create(
                table=self.model_log_name,
                description=self.dict_to_string(request.DATA),
                user=request.user,
                type=LOG_MODIFIED
            )
        return response

    def destroy(self, request, *args, **kwargs):
        # Add reservation register to data
        data = {'reservation': Reservation.objects.get(id=int(get_id(request.path)))}

        # handle framework response
        response = super(ModelViewSetLog, self).destroy(request, *args, **kwargs)

        # save log register of action
        if response.status_code == 204:
            Log.objects.create(
                table=self.model_log_name,
                description=self.dict_to_string(data),
                user=request.user,
                type=LOG_DELETED
            )
        return response

