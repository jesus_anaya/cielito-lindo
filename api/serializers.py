from rest_framework import serializers
from booking.models import Reservation, Room


class ReservationSerializer(serializers.ModelSerializer):
    nights = serializers.Field(source='nights')
    room_name = serializers.Field(source='room_name')
    user_name = serializers.Field(source="user")

    class Meta:
        model = Reservation


class RoomSerializer(serializers.ModelSerializer):
    room_type_name = serializers.Field(source='room_type_name')

    class Meta:
        model = Room
