from fabric.api import env, local, run, cd, sudo, prefix
from fabric.contrib.files import upload_template
from contextlib import contextmanager
from sani_booking.local_settings import FABRIC


env.hosts = FABRIC['hosts']
env.user = FABRIC['user']
env.password = FABRIC['password']
env.port = FABRIC['port']
env.venv_home = FABRIC['home_path']
env.venv_path = "%s/venv" % env.venv_home
env.pip = "%s/bin/pip" % env.venv_path
env.manage = "%s/bin/python %s/manage.py" % (env.venv_path, env.venv_home)

socketio = FABRIC['socketio_path']

# Templates definition
templates = {
    "nginx": {
        "local_path": "deploy/nginx.conf",
        "remote_path": "/etc/nginx/sites-enabled/sani_booking",
        "reload_command": "service nginx reload",
    },
    "local_settings": {
        "local_path": "deploy/local_settings.py",
        "remote_path": "%s/sani_booking/local_settings.py" % env.venv_home,
    },
    "supervisor": {
        "local_path": "deploy/supervisor.conf",
        "remote_path": "/etc/supervisor/conf.d/sani_booking.conf",
        "reload_command": "supervisorctl reload",
    }
}

def upload_templates():
    for name in templates:
        template = templates.get(name);
        local_path = template.get('local_path')
        remote_path = template.get('remote_path')

        # Upload template to host
        if local_path and remote_path:
            upload_template(local_path, remote_path, env, use_sudo=True, backup=False)


def restart_services():
    for name in templates:
        template = templates.get(name);
        reload_command = template.get('reload_command')

        # Seload Server
        if reload_command:
            sudo(reload_command)


def init_ssh():
    home_ssh = local("echo $HOME/.ssh/id_rsa", capture=True)
    env.key_filename = home_ssh


def tpls():
    init_ssh()
    upload_templates()
    restart_services()


def deploy():
    init_ssh()
    with cd(env.venv_home):
        run("git pull origin master")
        run("%s install -r requirements/deploy.txt" % env.pip)
        run("%s migrate --all" % env.manage)
        run("%s collectstatic --noinput" % env.manage)
    restart_services()
